
# Find image for EC2 instance
data "aws_ami" "amazon_linux_2" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["al2023-ami-2023*"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

# Local Vars
locals {
  hostname = format("%s-%s", var.environment, var.machine_name) # Hostname for EC2 instance
  key_name = format("%s-key", local.hostname) # key name for EC2 instance

  tags = { # Tags for resources
    Terraform   = "true"
    Environment = var.environment
  }
}

# Create Key Pair
module "key_pair" {
  source             = "terraform-aws-modules/key-pair/aws"
  key_name           = local.key_name
  create_private_key = true
}

# Create Private Key
resource "tls_private_key" "ssh" {
  algorithm = "RSA"
  rsa_bits  = "4096"
}

# Create security group for access EC2 instance from ssh protocol
module "ssh_security_group" {
  source = "terraform-aws-modules/security-group/aws"

  name        = format("%s-sg", local.hostname)
  description = format("Security group for %s EC2 instance.", local.hostname)
  vpc_id      = var.vpc_id

  # List of ingress rules and CIDR Block
  ingress_cidr_blocks = ["0.0.0.0/0"] # Anywhere
  ingress_rules       = ["ssh-tcp"] # SSH protocol (Port 22)

  # List of egress rules to create by name open to all-all
  egress_rules = ["all-all"] # All traffic

  tags = local.tags
}

# Create template startup script for EC2 instance
data "template_file" "startup_script" {
  template = file("${path.module}/startup-script.sh")

  vars = {
    HOSTNAME             = local.hostname
    PRIVATE_KEY          = tls_private_key.ssh.private_key_openssh
    PUBLIC_KEY           = tls_private_key.ssh.public_key_openssh
    ADDITIONAL_USER_DATA = var.user_data
  }
}

# Create EC2 instance
# https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/instancedata-data-categories.html
module "ec2_instance" {
  source = "terraform-aws-modules/ec2-instance/aws"

  name                        = local.hostname
  key_name                    = module.key_pair.key_pair_name
  ami                         = data.aws_ami.amazon_linux_2.id
  instance_type               = var.machine_type
  monitoring                  = true
  availability_zone           = var.availability_zone
  subnet_id                   = var.vpc_subnet
  vpc_security_group_ids      = [module.ssh_security_group.security_group_id]
  associate_public_ip_address = true
  user_data_base64            = base64encode(data.template_file.startup_script.rendered)
  user_data_replace_on_change = true
  tags                        = local.tags
  metadata_options = {
    "hostname" : local.hostname,
  }

}

# Create generict security group for access via ssh EC2 instances from bastion instance
module "bastion_ssh_security_group" {
  source = "terraform-aws-modules/security-group/aws"

  name        = format("%s-ssh-sg", local.hostname)
  description = format("Security group for ingress from %s instance.", local.hostname)
  vpc_id      = var.vpc_id

  ingress_cidr_blocks = ["${module.ec2_instance.private_ip}/32"] # Allow only bastion instance
  ingress_rules       = ["ssh-tcp"] # SSH protocol (Port 22)
  egress_rules        = ["all-all"]

  tags = local.tags
}
