output "key" {
  value = tls_private_key.ssh.public_key_openssh
}

output "ssh_security_group_id" {
  value = module.bastion_ssh_security_group.security_group_id
}
