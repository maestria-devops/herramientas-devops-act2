#!/bin/bash
sudo hostnamectl set-hostname ${HOSTNAME}

echo '${PRIVATE_KEY}' > /home/ec2-user/.ssh/id_rsa
echo '${PUBLIC_KEY}' > /home/ec2-user/.ssh/id_rsa.pub

sudo yum update -y

${ADDITIONAL_USER_DATA}