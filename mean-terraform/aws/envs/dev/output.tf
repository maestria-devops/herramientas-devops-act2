output "availability_zones" {
  value = module.network.vpc_azs
}

output "subnets" {
  value = module.network.vpc_private_subnets
}
