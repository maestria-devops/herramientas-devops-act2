
locals {
  # Get image id from AWS Console when it's created
  region = "us-east-1"
  linux_image_id = "ami-0d0efb8f3295db6bf" # MEAN AMI
  availability_zones = ["a", "b", "c"]
  environment = "dev-mean-app"  
}

provider "aws" {
  region = local.region
}

# VPC and Network
module "network" {
  source = "../../network"

  region             = local.region
  environment        = local.environment
  availability_zones = local.availability_zones
}

# Bastion Instance
module "vm_bastion" {
  source = "../../vm-bastion"

  region            = local.region
  environment       = local.environment
  availability_zone = module.network.vpc_azs["a"]
  vpc_id            = module.network.vpc_id
  vpc_subnet        = module.network.vpc_public_subnets["a"]
}

# MEAN instances
module "vm_mean" {  
  source = "../../vm-ami-image"

  count = length(local.availability_zones)

  region                        = local.region
  environment                   = local.environment
  machine_name                  = "instance-${local.availability_zones[count.index]}"
  machine_type                  = "t2.nano"
  machine_ami_id                = local.linux_image_id
  availability_zone             = module.network.vpc_azs[local.availability_zones[count.index]]
  vpc_id                        = module.network.vpc_id
  vpc_subnet                    = module.network.vpc_public_subnets[local.availability_zones[count.index]]
  associate_public_ip_address   = false
  ingress_cidr_blocks           = ["0.0.0.0/0"]
  ingress_rules                 = ["http-80-tcp"]
  bastion_key                   = module.vm_bastion.key
  bastion_ssh_security_group_id = module.vm_bastion.ssh_security_group_id
}

# Attach EC2 instances to Load Balancer
resource "aws_lb_target_group_attachment" "lb_target_group_a" {
  count = length(module.vm_mean)

  target_group_arn = module.network.alb_target_group_arn
  target_id        = module.vm_mean[count.index].id
  port             = 80
}

