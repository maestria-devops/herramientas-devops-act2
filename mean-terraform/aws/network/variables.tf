variable "environment" {
  description = "Environment name"
}

variable "region" {
  type        = string
  description = "The region to use"
}

#data "aws_availability_zones" "az" {}

variable "availability_zones" {
  type = list(string)

  validation {
    condition = alltrue([
      for az in var.availability_zones : contains(["a", "b", "c", "d", "e", "f"], az)
    ])
    error_message = "Invalid Availability Zones"
  }
}
