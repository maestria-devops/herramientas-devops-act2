# Local Vars
locals {
  vpc_name = format("%s-vpc", var.environment) # VPC Name
  vpc_cidr = "10.0.0.0/16" # Network segment
  azs      = [for k, v in var.availability_zones : "${var.region}${v}"]
  alb_name = format("%s-alb", var.environment) # Load balancer name

  tags = { # Tags for resources
    Terraform   = "true"
    Environment = var.environment
  }
}

# Create VPC (Virtual Private Cloud)
module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "5.4.0"

  name = local.vpc_name
  cidr = local.vpc_cidr

  azs             = local.azs
  private_subnets = [for k, v in local.azs : cidrsubnet(local.vpc_cidr, 8, k)]
  public_subnets  = [for k, v in local.azs : cidrsubnet(local.vpc_cidr, 8, k + 4)]

  # Single NAT Gateway
  enable_nat_gateway     = true
  single_nat_gateway     = true
  one_nat_gateway_per_az = true

  tags = local.tags
}

# Create  Load Balancer
module "alb" {
  source  = "terraform-aws-modules/alb/aws"
  version = "9.4.0"

  name    = local.alb_name
  vpc_id  = module.vpc.vpc_id
  subnets = module.vpc.public_subnets

  # For example only
  enable_deletion_protection = false

  #security_group_use_name_prefix = false
  security_group_name = format("%s-sg", local.alb_name)

  # Security Group for ingress traffic
  security_group_ingress_rules = {
    all_http = {
      from_port   = 80
      to_port     = 80
      ip_protocol = "tcp"
      description = "HTTP Web Traffic"
      cidr_ipv4   = "0.0.0.0/0" # Anywhere
    }
  }

  # Security Group for egress traffic
  security_group_egress_rules = {
    all = {
      from_port   = 80
      to_port     = 80
      ip_protocol = "tcp"
      description = "HTTP Web Traffic"
      cidr_ipv4   = local.vpc_cidr
    }
  }

  # Listener component that forward traffic from HTTP:80 to EC2 instances
  listeners = {
    http = {
      port     = 80
      protocol = "HTTP"
      forward = {
        target_group_key = "ec2-instances"
      }
    }
  }

  # Definition for target groups
  target_groups = {
    ec2-instances = {
      name        = format("%s-alb-tg", var.environment)
      protocol    = "HTTP"
      port        = 80
      target_type = "instance"
      create_attachment = false      

      health_check = {
        enabled             = true        
        path                = "/"
        matcher             = "200-399"
        interval            = 10
        timeout             = 6
        healthy_threshold   = 2
        unhealthy_threshold = 2
      }
    }
  }

  tags = local.tags
}
