const http = require('http');
const os = require('os');

const hostname = 'localhost';
const port = 3000;
let serverCount = 1;

const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');   

    res.end(
        JSON.stringify({
            MEAN_SERVER_NAME: os.hostname(),
            MEAN_SERVER_COUNT: serverCount,
            MONGODB_CONNECTION_STRING: 'mongodb+srv://<username>:<password>@<host>/<dbname>?retryWrites=true&w=majority'
        },null,'\t')
    )
    serverCount++;
});

server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});