#!/bin/bash

NODE_MAJOR=20
KEYRINGS_PATH="/usr/share/keyrings"

# Update and install required libs
sudo apt-get update && sudo apt-get install -y ca-certificates gnupg

# Update linux repository for NodeJS and NPM
wget -qO - https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | sudo gpg --dearmor -o $KEYRINGS_PATH/nodesource.gpg

echo \
    "deb [signed-by=$KEYRINGS_PATH/nodesource.gpg] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main" \
    | sudo tee /etc/apt/sources.list.d/nodesource.list

# Install NodeJS and Nginx
sudo apt-get update && sudo apt-get install nodejs build-essential nginx -y

# Enabled Nginx service
sudo systemctl enable nginx
sudo systemctl start nginx

# Install PM2
sudo npm install pm2@latest -g

# Check versions
echo "Node Version $(node -v)"
echo "NPM Version $(npm -v)"


