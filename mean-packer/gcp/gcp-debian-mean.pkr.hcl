packer {
  required_plugins { # Plugin to use
    googlecompute = { # Google Plataform
      version = ">= 1.1.1"
      source  = "github.com/hashicorp/googlecompute"
    }
  }
}

variable "project_id" { # Project Id for GCP
  type = string
}

variable "zone" { # Zone for GCP
  type = string
}

variable "builder_sa" { # Credentiales
  type    = string
  default = env("GOOGLE_APPLICATION_CREDENTIALS")
}

source "googlecompute" "debian" { # Source from GCP
  project_id                  = var.project_id
  zone                        = var.zone
  source_image                = "debian-12-bookworm-v20231115" #  Source image in GCP
  image_name                  = "mean-packer-debian-gcp" # Final name of image in GCP
  image_description           = "mean-packer-debian-gcp"
  ssh_username                = "gcp-user" # User for run script
  tags                        = ["packer"]
  service_account_email       = var.builder_sa # Service Account for execute packer
  scopes = [ # Security Scopes
    "https://www.googleapis.com/auth/compute",
    "https://www.googleapis.com/auth/devstorage.read_only"
  ]

}

build { # Build Image
  name = "mean-packer"
  sources = [
    "source.googlecompute.debian"
  ]


  provisioner "file" { # Copy Scripts to install y configure mean app
    source      = "../mean"
    destination = "/home/gcp-user/"
  }

  provisioner "shell" { # Execute scripts to install y configure mean app
    environment_vars = [

    ]
    inline = [
      "ls -l /home/gcp-user",
      "cd /home/gcp-user/mean/",
      "./install.sh",
      "./configure.sh"
    ]
  }
}

# https://www.linkedin.com/pulse/how-create-infrastructure-code-iaac-packer-terraform-google-chandio/

# https://github.com/GoogleCloudPlatform/cloud-builders-community/tree/master/packer/examples/gce