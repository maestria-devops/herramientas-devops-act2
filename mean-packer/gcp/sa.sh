gcloud iam service-accounts create packer \
  --project master-devops \
  --description="Packer Service Account" \
  --display-name="Packer Service Account"


gcloud projects add-iam-policy-binding master-devops \
    --member=serviceAccount:packer@master-devops.iam.gserviceaccount.com \
    --role=roles/compute.instanceAdmin.v1

gcloud projects add-iam-policy-binding master-devops \
    --member=serviceAccount:packer@master-devops.iam.gserviceaccount.com \
    --role=roles/iam.serviceAccountUser


gcloud projects add-iam-policy-binding master-devops \
    --member=serviceAccount:packer@master-devops.iam.gserviceaccount.com \
    --role=roles/iap.tunnelResourceAccessor